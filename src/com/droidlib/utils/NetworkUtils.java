package com.droidlib.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author LingChao
 */
public class NetworkUtils {

	public static boolean checkNetworkState(Context context) {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (manager.getActiveNetworkInfo() != null) {
			return manager.getActiveNetworkInfo().isConnected();
		}
		return false;
	}
	
	public static boolean isWifiContected(Context context){
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi=manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
		return wifi!=null&&wifi.isAvailable();
	}
}
