package com.droidlib.utils;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * @author LingChao
 */
public class MediaUtils {

	public static void notifyToScan(Context context,File file){
		Intent intent=new Intent(file.isDirectory()?"android.intent.action.MEDIA_SCANNER_SCAN_DIR":Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		intent.setData(Uri.fromFile(file));
		context.sendBroadcast(intent);
	}
}
