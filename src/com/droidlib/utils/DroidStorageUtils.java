package com.droidlib.utils;

import java.io.File;

import android.os.Environment;
import android.os.StatFs;

/**
 * @author LingChao
 */
public class DroidStorageUtils {

	public static long getSDAvailaleSize() {
		File path = Environment.getExternalStorageDirectory();
		StatFs stat = new StatFs(path.getPath());
		int blockSize = stat.getBlockSize();
		int availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}
	
	public static boolean isSdCardOK() {
		
		return Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
	}
}
