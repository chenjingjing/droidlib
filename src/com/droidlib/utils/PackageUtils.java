package com.droidlib.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

/**
 * @author Administrator
 */
public class PackageUtils {
	
	public static PackageInfo getPackageInfo(Context context,String pkg){
		PackageManager pm=context.getPackageManager();
		try {
			return pm.getPackageInfo(pkg, 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean isAppInstalled(Context context,String pkg){
		return getPackageInfo(context, pkg)!=null;
	}
}
