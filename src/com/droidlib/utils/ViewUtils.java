package com.droidlib.utils;

import android.app.Activity;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.widget.ListView;

/**
 * @author LingChao
 */
public class ViewUtils {

	public static void listViewSmoothScrollToPosition(final Activity activity,
            final ListView listView, final int position) {
        // Workarond: delay-call smoothScrollToPosition()
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (activity.isFinishing()) {
                    return; // Activity being destroyed
                }
                listView.smoothScrollToPositionFromTop(position,0);
            }
        });
	}
	
	
	public static int[] getScreenSize(Activity activity){
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		
		int[] ret=new int[2];
		ret[0]=dm.widthPixels;
		ret[1]=dm.heightPixels;
		
		return ret;
	}
}
